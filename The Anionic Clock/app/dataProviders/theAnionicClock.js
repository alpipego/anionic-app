var provider,
    TelerikBackendServices = require('../everlive/everlive.all.min');

provider = new TelerikBackendServices({

    appId: 'pfabjzx00fxk5rpa',
    scheme: 'https'
});

// START_CUSTOM_CODE_theAnionicClock
// Add custom code here. For more information about custom code, see http://docs.telerik.com/platform/screenbuilder/troubleshooting/how-to-keep-custom-code-changes

// END_CUSTOM_CODE_theAnionicClock
module.exports = provider;